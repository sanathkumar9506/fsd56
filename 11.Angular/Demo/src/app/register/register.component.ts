import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  empName: any;
  salary: any;
  gender: any;
  doj: any;
  country: any;
  phoneNumber: any;
  emailId: any;
  password: any;
  countries: any;

  constructor(private router: Router, private toastr: ToastrService,private service: EmpService) {
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data:any)=>{
      this.countries=data;
      console.log(data);
    });
  }


  submit() {
    console.log("EmpName: " + this.empName);
    console.log("Salary: " + this.salary);
    console.log("Gender: " + this.gender);
    console.log("DateOfJoin: " + this.doj);
    console.log("Country: " + this.country);
    console.log("PhoneNumber: " + this.phoneNumber);
    console.log("Email-Id: " + this.emailId);
    console.log("Password: " + this.password);
  }

  registerSubmit(regForm: any) {
console.log(regForm);
    if (!this.validateForm()) {
      return;
    }
    const newEmployee = {
      empId: this.generateEmpId(),
      empName: this.empName,
      salary: this.salary,
      gender: this.gender,
      doj: this.doj,
      country: this.country,
      phoneNumber: this.phoneNumber,
      emailId: this.emailId,
      password: this.password
    };

    console.log(newEmployee);

    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.router.navigate(['login']);
    
  }

  private validateForm(): boolean {

    if (!this.empName || !this.salary || !this.emailId || !this.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }

  private generateEmpId(): number {
    return Math.floor(Math.random() * 1000) + 1;
  }
}