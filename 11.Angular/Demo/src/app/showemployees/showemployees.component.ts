import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {
  
  employees: any;
  emailId: any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      console.log(data);
      this.employees = data;
    });
  }
}
