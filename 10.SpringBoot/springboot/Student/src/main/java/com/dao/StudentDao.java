package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	@Autowired
	StudentRepository studentRepository;

	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	public Student getStudentById(int rollNo) {
		Student student = new Student(0, "Student Not Found!!!", 0, "");
		return studentRepository.findById(rollNo).orElse(student);
	}

	public List<Student> getStudentByName(String studentName) {
		return studentRepository.findByName(studentName);
	}

	public Student addStudent(Student student) {
		return studentRepository.save(student);
	}

	public Student deleteStudent(int rollNo) {
		studentRepository.deleteById(rollNo);
		return null;
	}

	public Student updateStudent(Student updatedStudent) {
		int rollNo = updatedStudent.getRollNo();

		if (studentRepository.existsById(rollNo)) {
			return studentRepository.save(updatedStudent);
		} else {
			return null;
		}
	}

}
