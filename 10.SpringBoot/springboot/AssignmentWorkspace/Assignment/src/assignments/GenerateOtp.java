package assignments;

import java.util.Scanner;

public class GenerateOtp {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the first name,lastname and pincode");
		
		String fn = scan.next();
		String ln = scan.next();
		String pc = scan.next();
		
		String s4 = generateOtp(fn,ln,pc);
		
		System.out.println(s4);
	

	}
	
	public static String generateOtp(String str1,String str2,String str3){
		
		String result = " ";
		
		char ch = str2.charAt(str2.length()-1);
		
		ch = Character.toUpperCase(ch);

		result += ch;
		
		char ch2 = str1.charAt(0);
		ch2 = Character.toLowerCase(ch2);
		
		result += ch2;
		
		String str4 = str1.substring(1, str1.length());
		
		str4 = str4.toUpperCase();
		
		result += str4;
		
		char ch3 = str3.charAt(str3.length()-1);
		
		result += ch3;
		
		char ch4 = str3.charAt(0);
		
		result += ch4;
		
		
		return result;
	}

}